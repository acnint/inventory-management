// Ionic Starter App

//Global variables
var db = null;

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'ngCordova'])

/*DO NOT MODIFY*/
.run(function($ionicPlatform, $cordovaSQLite, $rootScope) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }

    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

    db = $cordovaSQLite.openDB({name: 'inventory_v1.db', location: 'default'}, function(res){ 
      console.log("Open database SUCCESS: " + JSON.stringify(res));
      alert("Open database SUCCESS: " + JSON.stringify(res));
    }, function(err){
      console.log("Open database ERROR: " + JSON.stringify(err));
      alert("Open database ERROR: " + JSON.stringify(err));
    });


    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS assets(id text, name text, type text, serialnumber text, assettag text, type2 text, serialnumber3 text, assettag4 text, manufacturer text, model text, assigneduser text, location text, corporateunit text, purchaser text, acquisitionmode text, deliverydate text, refreshdue text, contractnumber text, status text)").then(function(res){
      console.log("table assets created");
    }, function(err){
      console.log(err);
    });

    //For persistence testing purposes
    /*db.transaction(function(tx) {
      tx.executeSql('CREATE TABLE IF NOT EXISTS DemoTable (name, score)');
      tx.executeSql('INSERT INTO DemoTable VALUES (?,?)', ['Alice', 101]);
      tx.executeSql('INSERT INTO DemoTable VALUES (?,?)', ['Betty', 202]);
    }, function(error) {
      console.log('Transaction ERROR: ' + error.message);
    }, function() {
      console.log('Populated database OK');
    });

    db.transaction(function(tx) {
      tx.executeSql('SELECT count(*) AS mycount FROM DemoTable', [], function(tx, rs) {
        console.log('Record count (expected to be 2): ' + rs.rows.item(0).mycount);
      }, function(tx, error) {
        console.log('SELECT error: ' + error.message);
      });
    });*/

  });

  $rootScope.persistentFileExists;
})
/*DO NOT MODIFY*/

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('home', {url: "/home", templateUrl: "index.html", controller: 'MainCtrl'})
    .state('assetentry', {cache: false, url: '/assetentry/:barcodeid', templateUrl: 'asset-entry.html', controller: 'AssetEntryCtrl'})
    .state('assetlist', {cache: false, url: '/assetlist', templateUrl: 'asset-list.html', controller: 'AssetListCtrl'})
    .state('assetdetail', {cache: false, url: '/assetdetail/:barcodeid', templateUrl: 'asset-detail.html', controller: 'AssetDetailCtrl'});
  
  $urlRouterProvider.otherwise('/home');
}])

.controller('MainCtrl', function($scope, $cordovaBarcodeScanner, $cordovaFile, $cordovaSQLite, $state) {
  /*document.addEventListener('deviceready', function() {
    window.sqlitePlugin.echoTest(function() {
      console.log('Installation and build: OK');
    });
  });// SQLite Plugin: Testing installation and build*/

  /*document.addEventListener('deviceready', function() {
    window.sqlitePlugin.selfTest(function() {
      console.log('Basic database access operations: OK');
    });
  });*/ /*SQLite Plugin: Testing basic DB operations, such as opening a database; basic CRUD operations (create data in a table, 
          read the data from the table, update the data, and delete the data); close and delete the database*/
  
  $scope.launchBarCodeScanner = function(){
    document.addEventListener("deviceready", function () {
      scanBarCode();
    }, false);
  }; //launchBarCodeScanner

  $scope.goToAssetList = function(){
    console.log("goToAssetList");
    $state.go('assetlist');
  }; //goToAssetList

  var scanBarCode = function(){
      cordova.plugins.barcodeScanner.scan(
          function (result) {
              if(result.cancelled){
                alert("Scan cancelled");
                $state.go("home");
              }
              else{
                alert("We got a barcode\n" +
                    "Result: " + result.text + "\n" +
                    "Format: " + result.format + "\n" +
                    "Cancelled: " + result.cancelled);

                checkIfAssetExists(result.text);
              }
          },
          function (error) {
              alert("Scanning failed: " + error);
          },
          {
              preferFrontCamera : false, // iOS and Android
              showFlipCameraButton : true, // iOS and Android
              showTorchButton : true, // iOS and Android
              torchOn: false, // Android, launch with the torch switched on (if available)
              prompt : "Place a barcode inside the scan area", // Android
              resultDisplayDuration: 0, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
              //formats : "QR_CODE,PDF_417",  //default: all but PDF_417 and RSS_EXPANDED
              //orientation : "landscape", // Android only (portrait|landscape), default unset so it rotates with the device
              //disableAnimations : true // iOS
          }
       );
  }; //scanBarCode

  var checkIfAssetExists = function(bid){
    var query = "SELECT * FROM assets WHERE id = ?";
    console.log("Executing query: " + query + ", id: " + bid);

    db.readTransaction(function(tx) {
      tx.executeSql(query, [bid], function(tx, resultSet) {
        
        var resultLength = resultSet.rows.length;
        if(resultLength == 1) {
            alert("Asset Already Registered");
            $state.go('assetdetail', {barcodeid: bid});
        } else if(resultLength == 0) {
            console.log("No results found");
            alert("New Asset Scanned");
            $state.go('assetentry', {barcodeid: bid});
        } else {
            console.log("WTF Error! Number of results: " + resultLength);
            alert("WTF Error! Number of results: " + resultLength);
            $state.go('home');
        }
      }, function(tx, error) {
        console.log('SELECT error: ' + error.message);
        alert('SELECT error: ' + error.message);
        $state.go('home');
      });
    }, function(error) {
      console.log('transaction error: ' + error.message);
      alert('transaction error: ' + error.message);
      $state.go('home');
    }, function() {
      console.log('transaction ok');
    });
  }; //checkIfAssetExists

}) //MainCtrl

.controller('AssetEntryCtrl', function($scope, $cordovaSQLite, $state, $filter) {
  $scope.barcodeid = $state.params.barcodeid;

  $scope.registerAsset = function(asset){
    document.addEventListener("deviceready", function () {
      console.log("Asset name: " + asset.name);
      asset.deliverydate = $filter('date')(asset.deliverydate, 'yyyy-MM-dd');
      asset.refreshdue = $filter('date')(asset.refreshdue, 'yyyy-MM-dd');
      console.log("Delivery Date: " + asset.deliverydate);
      console.log("Refresh Due: " + asset.refreshdue);

      db.transaction(function(tx) {
        tx.executeSql('INSERT INTO assets VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [$scope.barcodeid, asset.name, asset.type, asset.serialnumber, asset.assettag, asset.type2, asset.serialnumber3, asset.assettag4, asset.manufacturer, asset.model, asset.assigneduser, asset.location, asset.corporateunit, asset.purchaser, asset.acquisitionmode, asset.deliverydate, asset.refreshdue, asset.contractnumber, '1']);
      }, function(error) {
        console.log('Transaction ERROR: ' + error.message);
        alert('Transaction ERROR: ' + error.message);
        $state.go('home');
      }, function() {
        console.log('New asset entry created: \nid: ' + $scope.barcodeid + "\nname: " + asset.name);
        alert('New asset entry created: \nid: ' + $scope.barcodeid + "\nname: " + asset.name);
        $state.go('assetdetail', {barcodeid: $scope.barcodeid});
      });
    }, false);
  }; //registerAsset

}) //AssetEntryCtrl

.controller('AssetDetailCtrl', function($scope, $cordovaSQLite, $state) {
  $scope.barcodeid = $state.params.barcodeid;

  document.addEventListener('deviceready', onDeviceReady, false);

  function onDeviceReady() {
    var query = "SELECT * FROM assets WHERE id = ?";
    console.log("Executing query: " + query + ", id: " + $scope.barcodeid);

    $scope.showEdit = true;
    $scope.editMode = false;
    $scope.ddate;
    $scope.rdue;

    db.readTransaction(function(tx) {
      tx.executeSql(query, [$scope.barcodeid], function(tx, resultSet) {
        var resultLength = resultSet.rows.length;
        if(resultLength == 1) {
            console.log("resultSet.rows.item(0).name: " + resultSet.rows.item(0).name);
            console.log("resultSet.rows.item(0).deliverydate: " + resultSet.rows.item(0).deliverydate);
            
            $scope.asset = resultSet.rows.item(0);
            $scope.ddate = new Date(resultSet.rows.item(0).deliverydate);
            $scope.rdue = new Date(resultSet.rows.item(0).refreshdue);
        } else if(resultLength == 0) {
            console.log("No results found");
            alert("No results found");
            $state.go('home');
        } else {
            console.log("WTF Error! Number of results: " + resultLength);
            alert("WTF Error! Number of results: " + resultLength);
            $state.go('home');
        }
      }, function(tx, error) {
        console.log('SELECT error: ' + error.message);
        alert('SELECT error: ' + error.message);
        $state.go('home');
      });
    }, function(error) {
      console.log('transaction error: ' + error.message);
      alert('transaction error: ' + error.message);
      $state.go('home');
    }, function() {
      console.log('transaction ok');
    });

    $scope.launchEditMode = function(){
      $scope.showEdit = false;
      $scope.editMode = true;
    }; //launchEditMode

    $scope.editAsset = function(asset){
      console.log("Editing asset w/ id: " + asset.id + ", name: " + asset.name);

      db.transaction(function (tx) {
          var query = "UPDATE assets SET name = ?, type = ?, serialnumber = ?, assettag = ?, type2 = ?, serialnumber3 = ?, assettag4 = ?, manufacturer = ?, model = ?, assigneduser = ?, location = ?, corporateunit = ?, purchaser = ?, acquisitionmode = ?, deliverydate = ?, refreshdue = ?, contractnumber = ?, status = ? WHERE id = ?";

          tx.executeSql(query, [asset.name, asset.type, asset.serialnumber, asset.assettag, asset.type2, asset.serialnumber3, asset.assettag4, asset.manufacturer, asset.model, asset.assigneduser, asset.location, asset.corporateunit, asset.purchaser, asset.acquisitionmode, asset.deliverydate, asset.refreshdue, asset.contractnumber, asset.status, asset.id], function(tx, res) {
              console.log("insertId: " + res.insertId);
              console.log("rowsAffected: " + res.rowsAffected);
          },
          function(tx, error) {
              console.log('UPDATE error: ' + error.message);
              alert('transaction error: ' + error.message);
          });
      }, function(error) {
          console.log('transaction error: ' + error.message);
          alert('transaction error: ' + error.message);
      }, function() {
          console.log("Asset " + asset.id + " updated successfully!");
          alert("Asset " + asset.id + " updated successfully!");
          
          //$state.go('assetdetail', {barcodeid: asset.id});

          $scope.showEdit = true;
          $scope.editMode = false;
          //$state.reload();
          $scope.$apply();
          //$route.reload(); //Remember to inject $route to your controller. 
        });
      
    }; //editAsset

    $scope.cancelEditMode = function(){
      $scope.showEdit = true;
      $scope.editMode = false;
    }; //cancelEditMode
  }

}) //AssetDetailCtrl

.controller('AssetListCtrl', function($scope, $cordovaSQLite, $state) {

  document.addEventListener('deviceready', onDeviceReady, false);

  function onDeviceReady() {
    var query = "SELECT * FROM assets"; //TODO: Pagination
    console.log("Executing query: " + query);

    db.readTransaction(function(tx) {
      tx.executeSql(query, [], function(tx, resultSet) {
        var resultLength = resultSet.rows.length;
        $scope.assets = [];
        if(resultLength > 0) {
            console.log("resultSet.rows.length: " + resultLength);

            for(var i=0; i<resultLength; i++){
              console.log("resultSet.rows.item(" + i + ").name: " + resultSet.rows.item(i).name);
              $scope.assets.push(resultSet.rows.item(i));
            }
        } else {
            console.log("No results found");
            alert("No results found");
            $state.go('home');
        }
      }, function(tx, error) {
        console.log('SELECT error: ' + error.message);
        alert('SELECT error: ' + error.message);
        $state.go('home');
      });
    }, function(error) {
      console.log('transaction error: ' + error.message);
      alert('transaction error: ' + error.message);
      $state.go('home');
    }, function() {
      console.log('transaction ok');
    });

    /*$cordovaSQLite.execute(db, query, []).then(function(res) {
        console.log("res.rows -> " + JSON.stringify(res.rows));
        console.log("res.rows.item -> " + JSON.stringify(res.rows.item));

        if(res.rows.length > 0) {
            alert("res.rows.length > 0");

            for(var i=0; i<res.rows.length; i++){
              console.log("SELECTED -> " + res.rows.item(i).id + " " + res.rows.item(i).assetname + "\n");
            }
        } else {
            console.log("No results found");
        }
    }, function (err) {
        console.log(err.message);
    });*/
  }

  $scope.goToAssetDetails = function(bid){
    console.log("goToAssetDetails: " + bid);
    $state.go('assetdetail', {barcodeid: bid});
  }; //goToAssetDetails

}); //AssetListCtrl